package self.push.dd.pigudfs;

import org.apache.pig.data.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by pgupta on 2/13/19.
 */
public class GET_WIKI_SUBDOMAIN_TEST {

    GET_WIKI_SUBDOMAIN getWikiSubdomain = new GET_WIKI_SUBDOMAIN();

    @Test
    public void test() throws Exception{
        Tuple tuple = TupleFactory.getInstance().newTuple(1);

        tuple.set(0,"en");
        assertEquals(".wikipedia.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"aa");
        assertEquals(".wikipedia.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"bla.bla.bla");
        assertEquals(".wikipedia.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"aa.b");
        assertEquals(".wikibooks.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"de.m.d");
        assertEquals(".wiktionary.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"xyz.f");
        assertEquals(".wikimediafoundation.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"de.m.m");
        assertEquals(".wikimedia.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"de.m.n");
        assertEquals(".wikinews.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"ar.q");
        assertEquals(".wikiquote.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"de.m.s");
        assertEquals(".wikisource.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"de.m.v");
        assertEquals(".wikiversity.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"de.m.voy");
        assertEquals(".wikivoyage.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"de.m.w");
        assertEquals(".mediawiki.org" , getWikiSubdomain.exec(tuple) );

        tuple.set(0,"de.m.wd");
        assertEquals(".wikidata.org" , getWikiSubdomain.exec(tuple) );
    }
}
