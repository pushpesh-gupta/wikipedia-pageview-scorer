package self.push.dd.pigudfs;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;

/**
 * Created by pgupta on 2/13/19.
 */
public class GET_WIKI_SUBDOMAIN extends EvalFunc<String> {

    private Map<String,String> codeToDomainMap;

    public GET_WIKI_SUBDOMAIN(){
        init();
    }

    @Override
    public String exec(Tuple input) throws IOException {

        if (input.size() != 1) { return null; }
        try {
            String domainCode = (String) input.get(0);
            return getDomain(domainCode);
        }
        catch (Exception e) {
            //TODO: what shall we do on exceptions? For now return null
            return null;
        }
    }

    private String getDomain(String domainCode){

        int lastIndexOfPeriod = domainCode.lastIndexOf('.');
        if(lastIndexOfPeriod == -1 || lastIndexOfPeriod == domainCode.length()-1){
            return codeToDomainMap.get("default");
        }else{
            String domainCodeSuffix = domainCode.substring(lastIndexOfPeriod+1);
            if(codeToDomainMap.containsKey(domainCodeSuffix)){
                return codeToDomainMap.get(domainCodeSuffix);
            }else{
                return codeToDomainMap.get("default");
            }
        }
    }

    private void init(){
        codeToDomainMap = new HashMap<String,String>();
        codeToDomainMap.put("default", ".wikipedia.org");
        codeToDomainMap.put("b", ".wikibooks.org");
        codeToDomainMap.put("d", ".wiktionary.org");
        codeToDomainMap.put("f", ".wikimediafoundation.org");
        codeToDomainMap.put("m", ".wikimedia.org");
        codeToDomainMap.put("n", ".wikinews.org");
        codeToDomainMap.put("q", ".wikiquote.org");
        codeToDomainMap.put("s", ".wikisource.org");
        codeToDomainMap.put("v", ".wikiversity.org");
        codeToDomainMap.put("voy", ".wikivoyage.org");
        codeToDomainMap.put("w", ".mediawiki.org");
        codeToDomainMap.put("wd", ".wikidata.org");
    }

}