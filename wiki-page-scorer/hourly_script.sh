#!/bin/bash
#
# script name ....: hourly_script.sh
#

function print_help() {
    echo "$0"
    echo "Usage is:"
    echo " "
    echo " --date   	: date in format YYYYMMDD. default is current date"
    echo " --hour   	: hour in format 00-23. default is current hour"
    echo " --data_dir   : base dir to keep raw data, status data and o/p data. default is /tmp"
    echo " "
    exit 1
}

BASEDIR=$(dirname "$0")
date=$(date +%Y%m%d)
hour=$(date +%H)
DATA_DIR="/tmp"

while [[ $# -gt 0 ]]; do
  case $1 in
    --help )
        print_help
        ;;

    --date=*)
        param=$(echo $1 | sed -e 's/^[^=]*=//g')
        [ "X" = "X"$param ] && print_help "invalid date!"
        date=${param}
        ;;

    --hour=*)
        param=$(echo $1 | sed -e 's/^[^=]*=//g')
        [ "X" = "X"$param ] && print_help "invalid hour!"
        hour=${param}
        ;;
    --data_dir=*)
        param=$(echo $1 | sed -e 's/^[^=]*=//g')
        [ "X" = "X"$param ] && print_help "invalid data directory!"
        DATA_DIR=${param}        
		;;
    *)
	# no more options, get on with life
        print_help "Unknown parameter $1!"
        ;;

  esac
  shift		# otherwise if no-match not shifted, would cause infinite loop
done


# set up work directories
TODODIR=${DATA_DIR}/todo
INPROGRESSDIR=${DATA_DIR}/in_progress
ARCHIVEDIR=${DATA_DIR}/archive
RAWDATADIR=${DATA_DIR}/raw_data
RESULTSDIR=${DATA_DIR}/results

if test ! -d $TODODIR; then
    mkdir -p $TODODIR;
fi
if test ! -d $INPROGRESSDIR; then
    mkdir -p $INPROGRESSDIR;
fi
if test ! -d $ARCHIVEDIR; then
    mkdir -p $ARCHIVEDIR;
fi
if test ! -d $RAWDATADIR; then
    mkdir -p $RAWDATADIR;
fi
if test ! -d $RESULTSDIR; then
    mkdir -p $RESULTSDIR;
fi

# get date parts
DATE=${date}
P_DATE=$(date -d "$DATE")
YEAR=$(date -d "$P_DATE" +%Y)
MONTH=$(date -d "$P_DATE" +%m)
DAY=$(date -d "$P_DATE" +%d)

# check if date-hour has been processed or not: TODO
status_file_name=${date}-${hour}

if test -f $ARCHIVEDIR/${status_file_name};
then
	echo "INFO: Processing for ${date} and ${hour} is already done!"
	exit 1
fi

# download file
raw_gz_file_name=pageviews-${date}-${hour}0000.gz

download_url="https://dumps.wikimedia.org/other/pageviews/${YEAR}/${YEAR}-${MONTH}/${raw_gz_file_name}"
echo " downlading file via ${download_url}"

curl -o ${RAWDATADIR}/${raw_gz_file_name} ${download_url}

if [ $? -ne 0 ]
then
  echo "ERROR: During download of file ${raw_gz_file_name} via ${download_url}"
  exit 1
fi

# unzip file
gunzip -f -d ${RAWDATADIR}/${raw_gz_file_name}

if [ $? -ne 0 ]
then
  echo "ERROR: During unzip of file ${raw_gz_file_name}."
  exit 1
fi

raw_file_name=pageviews-${date}-${hour}0000

dataFile=${RAWDATADIR}/${raw_file_name}
greylistFile=${BASEDIR}/conf/blacklist_domains_and_pages
outDir=${RESULTSDIR}/${date}-${hour}

#  call the main program to find top 25 pages
echo "processing file $dataFile...."

pig -param dataFile="${dataFile}" -param greylistFile="${greylistFile}" -param top_n=25 -param outDir="${outDir}" -x local ${BASEDIR}/pig/gl-page-scorer.pig

# Create a marker file to indicate date-hour which has been processed
touch $ARCHIVEDIR/${status_file_name}

# delete the data file from the disk
rm -rf ${RAWDATADIR}/${raw_file_name}



