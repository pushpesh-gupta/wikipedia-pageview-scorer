#!/bin/bash
#
# script name ....: batch_script.sh
#

function print_help() {
    echo "$0"
    echo "Usage is:"
    echo " "
    echo " --sdate   	: start date in format YYYYMMDD"
    echo " --shour   	: start hour in format 00-23"
    echo " --edate   	: end date in format YYYYMMDD"
    echo " --ehour   	: end hour in format 00-23"
    echo " --data_dir   : base dir to keep raw data, status data and o/p data. default is /tmp"
    echo " "
    exit 1
}

DATA_DIR=/tmp
while [[ $# -gt 0 ]]; do
  case $1 in
    --help )
        print_help
        ;;

    --sdate=*)
        param=$(echo $1 | sed -e 's/^[^=]*=//g')
        [ "X" = "X"$param ] && print_help "invalid date!"
        sdate=${param}
        ;;

    --shour=*)
        param=$(echo $1 | sed -e 's/^[^=]*=//g')
        [ "X" = "X"$param ] && print_help "invalid hour!"
        shour=${param}
        ;;
    --edate=*)
        param=$(echo $1 | sed -e 's/^[^=]*=//g')
        [ "X" = "X"$param ] && print_help "invalid date!"
        edate=${param}
        ;;

    --ehour=*)
        param=$(echo $1 | sed -e 's/^[^=]*=//g')
        [ "X" = "X"$param ] && print_help "invalid hour!"
        ehour=${param}
        ;;        
    --data_dir=*)
        param=$(echo $1 | sed -e 's/^[^=]*=//g')
        [ "X" = "X"$param ] && print_help "invalid data directory!"
        DATA_DIR=${param}        
		;;
    *)
	# no more options, get on with life
        print_help "Unknown parameter $1!"
        ;;

  esac
  shift		# otherwise if no-match not shifted, would cause infinite loop
done

BASEDIR=$(dirname "$0")
SDATE=$(date -d"${sdate} ${shour}:00:00")
EDATE=$(date -d"${edate} ${ehour}:00:00")

while [ "${SDATE}" != "${EDATE}" ] ; 
do 
        SDATE=$(date -d "${SDATE} + 1 hour")
	P_DATE=$(date +%Y%m%d -d "${SDATE}")
        P_HOUR=$(date +%H -d "${SDATE}")
        echo "Running ${BASEDIR}/hourly_script.sh --date=${P_DATE} --hour=${P_HOUR} --data_dir=${DATA_DIR}"
	${BASEDIR}/hourly_script.sh --date=${P_DATE} --hour=${P_HOUR} --data_dir=${DATA_DIR}	
	
done


