
REGISTER 'lib/push-pigudfs-1.0-SNAPSHOT.jar';

DEFINE GET_WIKI_SUBDOMAIN self.push.dd.pigudfs.GET_WIKI_SUBDOMAIN();

-- load unfiltered data rows
raw_data_rows_nf = LOAD '$dataFile' USING PigStorage(' ') AS (domain_code:chararray, page_title:chararray, count_views:int, total_response_size:int);

-- load grey list data
gl_data_rows = LOAD '$greylistFile' USING PigStorage(' ') AS (domain_code:chararray, page_title:chararray);

-- cogroup two datasets
cgrpd_data = COGROUP raw_data_rows_nf BY (domain_code, page_title), gl_data_rows BY (domain_code, page_title);

-- filter out rows which matched grey list
raw_data_rows_filtered = FILTER cgrpd_data BY NOT IsEmpty(raw_data_rows_nf) AND IsEmpty(gl_data_rows);

raw_data_rows_filtered_g = FOREACH raw_data_rows_filtered GENERATE FLATTEN(group) , SUM(raw_data_rows_nf.count_views) as count_views;

-- decode domain_code to wiki sub-subdomain to prepare data for sub-domain level aggregation
raw_data_rows = FOREACH raw_data_rows_filtered_g GENERATE
		GET_WIKI_SUBDOMAIN(domain_code) as domain,
		page_title as page,
		count_views as count;

group_by_domain_and_page = GROUP raw_data_rows BY (domain, page) ;

data_grouped_by_domain_and_page = FOREACH group_by_domain_and_page GENERATE flatten(group), SUM(raw_data_rows.count) as view_count;

-- DUMP data_grouped_by_domain_and_page;

data_group_by_domain = GROUP data_grouped_by_domain_and_page BY domain;

topResults = FOREACH data_group_by_domain {

	-- and retain top N (in ascending order) occurrences of 'page' for a 'domain'
	result = TOP($top_n, 2, data_grouped_by_domain_and_page);

	GENERATE FLATTEN(result);
}

topResultsSorted = ORDER topResults BY domain ASC, view_count DESC;

RMF outDir 
store topResultsSorted INTO '$outDir' USING PigStorage(' ');
