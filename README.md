# Wikipedia pageview data pipeline #

A simple application that computes the top 25 pages (by views) on Wikipedia for each of the Wikipedia sub-domains

### Specs ###

•	Accepts input parameters for the date and hour of data to analyze (default to the current date/hour if not passed).

•	Download the page view counts from wikipedia for the given date/hour in https://wikitech.wikimedia.org/wiki/Analytics/Data/Pagecounts-all-sites format from https://dumps.wikimedia.org/other/pageviews/.

•	Eliminate any pages found in the blacklist: conf/blacklist_domains_and_pages 

•	Compute the top 25 articles for the given day and hour by total pageviews for each unique domain in the remaining data.

•	Save the results to a file locally sorted by domain and number of pageviews for easy perusal.

•	Only run these steps if necessary; that is, not rerun if the work has already been done for the given day and hour.

•	Be capable of being run for a range of dates and hours; each hour within the range should have its own result file.


### Set up, system requirements and running ###

•	Checkout the repo. It has two modules, "wiki-page-scorer" and "push-pigudfs".

•	"push-pigudfs" is java/maven project with source code for Pig UDFs used in "wiki-page-scorer".

•	"wiki-page-scorer" has bash and pig executables for Linux machines

•	Linux machine with JRE 1.6 or higher, HadoopVersion 2.6.0-cdh5.8.3 or higher, PigVersion 0.12.0-cdh5.8.3 or higher.

Sample usage1:
./hourly_script.sh --date=20190210 --hour=18 –data_dir=/var/data/wiki/

Sample usage2: 
./batch_script.sh --sdate=20190214 --shour=21 --edate=20190215 --ehour=02 –data_dir=/var/data/wiki/

### Notes: ###

•	If data_dir is not provided then /tmp is default.

•	data_dir is used to download data files from wiki server, keep track of date-hour processed, and keep final results (pig script output)

•	Post processing, data files downloaded from wiki server are deleted from the disk.

•	In each successive run keep data_dir same to prevent re-processing of same date-hour

•	Pig script runs in local mode with single mapper and reducer, hence can take 5 to 10 minutes per date-hour

•	All the logs go to console, so be mindful when running for broader date ranges.

•	I could not understand explanation for domain code “.mw”, hence have skipped that in GET_WIKI_SUBDOMAIN. So pages with that domain code will default to “.wikipedia.org”
